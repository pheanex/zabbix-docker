Zabbix in a Docker Container

### What is this repository for? ###

* Contains the Zabbix Application for Monitoring
* Everything included is built from source (no rpm/deb packages taken)
* Currently uses Zabbix version 2.4.5 (But you can just change it in the Dockerfiles)
* The idea is to get a running Zabbix very easily (without much Configuration)

### How do I get set up? ###

To get a running Zabbix you can just run

	./create-zabbix-instance

and then you should be able to access your Zabbix instance under http://<YourHost> that's it.

If you want to change Stuff (like replacing my name "PHeanEX" with your you can just run:

	rgrep -i pheanex . 

and replace every occurrence with your name.

### Autostart Zabbix-server ###
To automatically start the zabbix server on boot and also restart both containers if they fail/stop
you can use the systemd service files.
Just copy them to your /etc/systemd/system directory.

	cp zabbix-*.service /etc/systemd/sytem/

Reload the configuration files for systemd with:

	systemctl daemon-reload

Enable the services with:

	systemctl enable zabbix-database.service
	systemctl enable zabbix-frontend.service

And to finally start them:

	systemctl start zabbix-frontend

You can take a look at the logs with
	journalctl -u -f zabbix-frontend.service

### Todos ###

* Enable https (Should be easy, just add your certificate to apache in zabbix-frontend)
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: it's me :-) bitbucket@pheanex.de
