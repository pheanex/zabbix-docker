#!/bin/bash

# Initialize the (postgres) database
# If you change username/pw here, don't forget to also change it in the frontend-container
gosu postgres postgres --single <<-EOF
	CREATE USER zabbix WITH PASSWORD 'zabbix';
	CREATE DATABASE zabbix;
	GRANT ALL PRIVILEGES ON DATABASE zabbix to zabbix;
EOF

# Start database
gosu postgres postgres -D /var/lib/postgresql/data/ &>/dev/null &

# Database needs some time to startup
sleep 5

# Initialize database with templates
psql -U zabbix zabbix < "/tmp/schema.sql"
psql -U zabbix zabbix < "/tmp/images.sql"
psql -U zabbix zabbix < "/tmp/data.sql"

# Stop database
kill -9 $(jobs -p)
rm /var/lib/postgresql/data/postmaster.pid

# Clean up
rm -rf /tmp/{schema.sql,images.sql,data.sql}
